'use strict';

describe('Controller: ToolCtrl', function () {

  // load the controller's module
  beforeEach(module('supportalApp'));

  var ToolCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ToolCtrl = $controller('ToolCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
