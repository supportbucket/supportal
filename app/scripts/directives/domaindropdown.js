'use strict';

/**
 * @ngdoc directive
 * @name supportalApp.directive:domainDropdown
 * @description
 * # domainDropdown
 */
angular.module('supportalApp')
  .directive('domainDropdown', function () {
    return {
      scope: {
        options: "=",
        serverUrl: "=ngModel"
      },
      templateUrl: 'views/directives/domainDropdown.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        function setSelectedUrlDisplayText(selectedUrl) {
          if (selectedUrl) {
            var selection = _.find(scope.options, function (option) {
              return option.url === selectedUrl;
            });
            scope.serverUrl = selection.url;
            scope.serverName = selection.name;
          } else {
            scope.serverUrl = scope.options[0].url;
            scope.serverName = scope.options[0].name;
          }
        }

        scope.setActiveDomain = function (domainUrl) {
          setSelectedUrlDisplayText(domainUrl);
        };

        setSelectedUrlDisplayText(scope.serverUrl);
      }
    };
  });
