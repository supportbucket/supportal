'use strict';

/**
 * @ngdoc directive
 * @name supportalApp.directive:loader
 * @description
 * # loader
 */
angular.module('supportalApp')
  .directive('loader', function () {
    return {
      templateUrl: 'views/directives/loader.html',
      restrict: 'E'
    };
  });
