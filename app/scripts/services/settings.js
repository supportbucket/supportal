'use strict';

/**
 * @ngdoc service
 * @name supportalApp.config
 * @description
 * # config
 * Service in the supportalApp.
 */
angular.module('supportalApp')
  .service('settings', function ($http, $q) {
    var def = $q.defer();
    this.init = function () {

      $http.get("settings/settings.json").then(function (data) {
        def.resolve(data), function (error) {
          def.reject(error);
        }
      });

      return def.promise;
    }
  });
