'use strict';

/**
 * @ngdoc service
 * @name supportalApp.utils
 * @description
 * # utils
 * Service in the supportalApp.
 */
angular.module('supportalApp')
  .service('utils', function ($http) {
    this.getSiteVersion = function (domain, siteid) {
      var url = 'https://' + domain + '/hc/web/public/pub/serverversion.jsp?siteid=' + siteid;
      //$httpProvider.defaults.headers.common['Content-Type'] = 'html/text';


      //$http.jsonp(url).then(function (result) {
      //  console.log(result);
      //}, function (error) {
      //  console.error("error getting site version", error);
      //})
      return url;
    }
  });
