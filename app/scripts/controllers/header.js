'use strict';

/**
 * @ngdoc function
 * @name supportalApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the supportalApp
 */
angular.module('supportalApp')
  .controller('HeaderCtrl', function ($scope, $cookies, $rootScope, captions) {
    $scope.captions = captions;
    $rootScope.personalFullName = $cookies.get("personalFullName") || captions.login;
  });
