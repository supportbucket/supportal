'use strict';

/**
 * @ngdoc overview
 * @name supportalApp
 * @description
 * # supportalApp
 *
 * Main module of the application.
 */
angular
  .module('supportalApp', [
    'ngCookies',
    'ngAnimate',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'cfp.hotkeys',
    'ngClipboard'
  ])
  .config(function ($routeProvider, $sceDelegateProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/tools.html',
        controller: 'ToolsCtrl'
      })
      .when('/error', {
        templateUrl: 'views/404.html'
      })
      .when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsCtrl'
      })
      .when('/tools', {
        templateUrl: 'views/tools.html',
        controller: 'ToolsCtrl'
      })
      .when('/chatApiTest', {
        templateUrl: 'views/chatapitest.html',
        controller: 'ChatAPITestCtrl'
      })
      .otherwise({
        redirectTo: '/error'
      });

    $sceDelegateProvider.resourceUrlWhitelist([
      'self', '**',
      'https://prodkb.tlv.lpnet.com:3000/**',
      'http://prodkb.tlv.lpnet.com:3001/**',
      'https://base.liveperson.net/**'
    ]);

  });
