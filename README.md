# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get Sky Go? ###

Sky Go is basically an online service where one can access Sky television services on their wireless devices such as smartphones and tablet. The service was launched in the year 2006 and one can watch on demand videos, live tv and much more on their mobile phone, pc, macbook, Xbox 360, Xbox One, PlayStation 3 and 4.  This service is available for no extra cost to the existing Sky tv customers whereas the non Sky tv subscribers will be charged according to pay per view basis. Normally, you can watch Sky Go on up to 2 devices but the devices can be increased to 4 with Sky Go Extra. You will have to pay a monthly fee for Sky Go Extra. To play the shows or tv’s on computer, you will require Microsoft Silverlight 3.0 browser on your system. In order to access the full selection of live TV channels on Sky Go, you will have to subscribe for the relevant tv packages that match up to the available channels. To learn more about Sky Go, feel free to get in touch with Sky customer services [via number listed here](http://www.followthesteps.net/sky-contact-phone-number/).

### Sky Go Extra ###

The major advantage of Sky Go Extra is that the users who are subscribed to Sky Go will easily able to download their favourite shows on their phone with double the number of devices connected to Sky broadband. This service is available at 

£5/month that allows four devices per every Sky Tv account. The app can be downloaded from the “App Store” as well as “Play Store”.

### Benefits of having Sky Go Extra ###

To know about the benefits of Sky Go Extra, kindly read the points given below:

*Independence to watch downloads anywhere

You can easily watch your saved shows without using internet. This is perfect for holidays or long trips. 

*Register up to four devices

Connect up to four devices and download shows on up to 4 compatible devices. 

*Watch Sky Tv on game consoles

Now enjoy the best of Sky tv on your most loved game consoles such as Xbox 360, PS3, PS4 and Xbox One. 

*Download Tv episodes 

Download your favourite tv shows and store it in your compatible smartphone, laptop or tablet.

### Get Sky Go Extra ###

If you [want to know about the packages of Sky](http://qwikfix.co.uk/sky-customer-services/) Go Extra, then go through the points given below:

*For Sky Tv Customers: If you are already a Sky Tv customer then you will get Sky Go Extra services free for 2 months and then you will be charged £5 extra per month. 

*For Sky Multiscreen Customers: If you are using Sky Multiscreen services, then you can subscribe for Sky Go Extra for free. 


Sky Go On Xbox One

The launch of Sky Go app on Xbox One means that the users can enjoy Sky Tv services on their favourite game consoles. Please note, that you will have to upgrade it to Sky Go Extra to use this service. It will cost not more than £5 per month and gives you a freedom to watch Sky Go on four devices. 

* Repo owner or admin
* Other community or team contact